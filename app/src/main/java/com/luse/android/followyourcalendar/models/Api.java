package com.luse.android.followyourcalendar.models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.luse.android.followyourcalendar.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by sarah-luse on 2/8/16.
 *
 * Clase para la gestión de las llamadas al servidor node y a la base de datos. Existen diferentes
 * metodos:
 * - post: guardar una tarea (nombre, horas totales, hora comienzo, hora fin...).
 * - login: loguear un usuario.
 * - getProjectTasks: get del listado de proyectos/tareas para inflar los spinners (por usuario).
 * - getAllUsers: get de todos los usuarios de la base de datos.
 * - getTasks: get de todas las tareas de un usuario.
 * - deleteTask: borrar una tarea.
 */

public class Api {

    public static String URL = "http://192.168.1.10:3000";
    public static String USER_DAY_URL = "/task/get/userday/";
    public static String DELETE_URL = "/task/delete/";
    public static String PROJECTS_USER_URL = "/project/get/";
    public static String TASKS_PROJECT_URL = "/tasklist/get/all/";
    public static final String LOGIN_URL = "/user/login";
    public static String TASK_MONTH_URL = "/task/search/usermonth/";
    public static String MONTH_URL = "/task/search/usermonth/";
    public static String YEAR_URL = "/task/search/useryear/";
    public static String ALL_PROJ_URL = "/project/all/";
    public static String ALL_TASKS_URL = "/tasklist/all/";
    public static String ALL_USERS = "/user/all";
    public static String GET_STATS = "/task/get/stats/";
    public static String GET_ALL_PROJECTS_USER = "/userproject/get/all/";
    //public static String URL = "http://192.168.0.17:3000";

    public Api() {

    };


    public void post(Task task, final Context context, String urlName, final Callback<Task> callback) {
        String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);

        Ion.with(context)
                .load(url)
                .setBodyParameter(context.getString(R.string.name), task.getName())
                .setBodyParameter(context.getString(R.string.project), task.getProject())
                .setBodyParameter(context.getString(R.string.hours), String.valueOf(task.getHours()))
                .setBodyParameter(context.getString(R.string.minutes), String.valueOf(task.getMinutes()))
                .setBodyParameter(context.getString(R.string.day), String.valueOf(task.getDay()))
                .setBodyParameter(context.getString(R.string.month), String.valueOf(task.getMonth()))
                .setBodyParameter(context.getString(R.string.year), String.valueOf(task.getYear()))
                .setBodyParameter(context.getString(R.string.user), String.valueOf(task.getUserId()))
                .setBodyParameter(context.getString(R.string.begintime), task.getBeginTime())
                .setBodyParameter(context.getString(R.string.endtime), task.getEndTime())
                .setBodyParameter(context.getString(R.string.projectId), String.valueOf(task.getprojectID()))
                .setBodyParameter(context.getString(R.string.taskid), String.valueOf(task.getTaskId()))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            callback.onCompleted(new Gson().fromJson(result, Task.class));
                            Toast.makeText(
                                    context,
                                    R.string.saved,
                                    Toast.LENGTH_SHORT
                            ).show();
                        } else {
                            Log.d(context.getString(R.string.ion), String.valueOf(e));
                            Toast.makeText(
                                    context,
                                    String.valueOf(e),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
    }

    public void login(String name, String password, final Context context, String urlName,
                      final Callback<String> callback) {

        String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);

        Ion.with(context)
                .load(url)
                .setBodyParameter(context.getString(R.string.user), name)
                .setBodyParameter(context.getString(R.string.pass), password)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        callback.onCompleted(result);

                    }
                });
    }

    public void getProjectTask(final Context context, String urlName,
                               final Callback<TaskProject> callback) {
        final String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            TaskProject taskProject = TaskProject.getTaskProjectListFromJson(result);
                            callback.onCompleted(taskProject);
                        } else {
                            Log.d("error", String.valueOf(e));
                        }
                    }
                });
    }

    public void getProjectTasks(final Context context, String urlName,
                                final Callback<ArrayList<TaskProject>> callback) {
        final String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            ArrayList tasksProjects = new ArrayList();
                            for (JsonElement item: result) {
                                tasksProjects.add(TaskProject.getTaskProjectListFromJson(item));
                            }
                            callback.onCompleted(tasksProjects);
                        } else {
                            Log.d("error", String.valueOf(e));
                        }
                    }
                });
    }

    public void getUserProjects(final Context context, String urlName,
                                final Callback<ArrayList<UserProject>> callback) {
        final String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            ArrayList<UserProject> userProjects = new ArrayList<>();
                            for (JsonElement item: result) {
                                userProjects.add(UserProject.getUserProjectListFromJson(item));
                            }
                            callback.onCompleted(userProjects);
                        } else {
                            Log.d("error", String.valueOf(e));
                        }
                    }
                });
    }

    public void getAllUsers(Context context, String urlName,
                            final Callback<ArrayList<User>> callback){

        String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            ArrayList<User> users = new ArrayList<User>();
                            for (JsonElement item: result) {
                                users.add(User.getUserFromJson(item));
                            }
                            callback.onCompleted(users);
                        }
                    }
                });
    }

    public void getTasks(Context context, String urlName, final Callback<ArrayList<Task>> callback) {
        String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            ArrayList<Task> tasks = new ArrayList<Task>();
                            for (JsonElement item: result){
                                tasks.add(Task.StorableTask.getTasksFromJson(item));
                            }
                            callback.onCompleted(tasks);
                        }

                    }
                });
    }

    public void deleteTask(Context context, String urlName, final Callback<String> callback) {
        String url = URL + urlName;
        Log.d(context.getString(R.string.ion), url);
        Ion.with(context)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        callback.onCompleted(result);
                    }
                });

    }

    public interface Callback<T> {
        void onCompleted(T result);
    }
}
