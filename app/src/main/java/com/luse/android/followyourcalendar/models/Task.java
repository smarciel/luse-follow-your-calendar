package com.luse.android.followyourcalendar.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

/**
 * Created by sarah-luse on 20/6/16.
 *
 * Clase que define una tarea.
 * Tiene una clase interna "StorableTask" que sirve para parsear instancias de la clase en formato
 * JSON a la clase Task.
 */

public class Task {
    private String name;
    private String beginTime;
    private String endTime;
    private String project;
    private int hours;
    private int minutes;
    private int day;
    private int month;
    private int year;
    private int UserId;
    private int id;
    private int taskId;
    private int projectId;

    public Task(String name, String project, String beginHour, String endHour) {
        this.name = name;
        this.project = project;
        this.beginTime = beginHour;
        this.endTime = endHour;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setprojectID(int projectID) {
        this.projectId = projectID;
    }

    public int getprojectID() {
        return projectId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String storeName() {
        return "/task/add";
    }

    public static class StorableTask {

        public static Task getTasksFromJson(JsonElement json) {
            return new Gson().fromJson(json, Task.class);
        }
    }
}
