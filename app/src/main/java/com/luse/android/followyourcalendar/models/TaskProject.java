package com.luse.android.followyourcalendar.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

/**
 * Created by sarah-luse on 4/8/16.
 *
 * Clase para definir un Proyecto o una Tarea a la hora de inflar los spinners.
 * Se usa para parsear de JSON a Java.
 */

public class TaskProject {
    private String name;
    private int id;

    public TaskProject() {

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static TaskProject getTaskProjectListFromJson(JsonElement json) {
        return new Gson().fromJson(json, TaskProject.class);
    }
}

