package com.luse.android.followyourcalendar.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

/**
 * Created by sarah-luse on 9/8/16.
 *
 * Clase que define un usuario de la aplicacion. Se utiliza para loguearse y mantener el id.
 */

public class User {
    private String username;
    private String surnames;
    private String password;
    private String email;
    private int id;

    public User() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getSurnames() {
        return surnames;
    }

    public String getUsername() {
        return username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static User getUserFromJson(JsonElement json) {
        return new Gson().fromJson(json, User.class);
    }
}
