package com.luse.android.followyourcalendar.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

/**
 * Created by sarah-luse on 5/9/16.
 */

public class UserProject {

    private int userid;
    private int projectid;

    public UserProject() {

    }

    public int getProjectid() {
        return projectid;
    }

    public int getUserid() {
        return userid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public static UserProject getUserProjectListFromJson(JsonElement json) {
        return new Gson().fromJson(json, UserProject.class);
    }
}
