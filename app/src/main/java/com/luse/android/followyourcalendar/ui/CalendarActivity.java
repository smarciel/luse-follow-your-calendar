package com.luse.android.followyourcalendar.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;
import com.luse.android.followyourcalendar.models.Task;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarActivity extends AppCompatActivity {
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    private int year;
    private int month;
    private int userid;
    private ArrayList<Integer> pairMonth;
    private ArrayList<Integer> oddMonth;
    private ArrayList<Integer> february;
    private Api api;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);

        final Bundle bundle = getIntent().getExtras();
        userid = bundle.getInt(MainActivity.USER_ID);
        pairMonth = new ArrayList<>();
        oddMonth = new ArrayList<>();
        february = new ArrayList<>();
        year = Calendar.getInstance().get(Calendar.YEAR);
        month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        api = new Api();

        //Inicializamos los Arrays con los dias de los meses para la funcionalidad Horas Totales.
        initializePairMonth();
        initializeOddMonth();
        initializeFebruary();

        //Tenemos un listener atento a los cambios en el calendario, para coger las fechas.
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int y, int m, final int dayOfMonth) {
                year = y;
                month = m + 1;

                //Cuando seleccione un dia, abrimos la nueva activity y le pasamos los datos del dia.
                startActivity(new Intent(getApplicationContext(), DayActivity.class)
                        .putExtra(DayActivity.DAY_TAG, dayOfMonth)
                        .putExtra(DayActivity.MONTH_TAG, month)
                        .putExtra(DayActivity.YEAR_TAG, year)
                        .putExtra(MainActivity.USER_ID, bundle.getInt(MainActivity.USER_ID)));
            }
        });
    }
    //Inicializamos el Array para los meses pares.
    private void initializePairMonth() {
        pairMonth.clear();
        for (int i = 1; i <= 30; i++) {
            pairMonth.add(i);
        }
    }
    //Inicializamos el Array para los meses impares.
    private void initializeOddMonth() {
        oddMonth.clear();
        for (int i = 1; i <= 31; i++) {
            oddMonth.add(i);
        }
    }
    //Inicializamos el Array para febrero.
    private void initializeFebruary() {
        february.clear();
        for (int i = 1; i <= 28; i++) {
            february.add(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Funcionalidad para calcular los dias pendientes de imputar las horas.
        //Recogemos todas las tareas de un usuario para un mes concreto y vamos restando los dias del
        //arraylist del mes que sea (par o impar).
        if (item.getItemId() == R.id.pending_days) {
            String url = Api.TASK_MONTH_URL + userid + "-" + month + "-" + year;
            api.getTasks(getApplicationContext(), url, new Api.Callback<ArrayList<Task>>() {
                @Override
                public void onCompleted(ArrayList<Task> result) {
                    if (result != null && !result.isEmpty()) {
                        int pos = 1;
                        for (Task item : result) {
                            Log.d("DAY", String.valueOf(item.getDay()));
                            if (isPair()) {
                                pairMonth.remove(item.getDay() - pos);
                            } else if (isOdd()) {
                                oddMonth.remove(item.getDay() - pos);
                            } else {
                                february.remove(item.getDay() - pos);
                            }
                            pos++;
                        }
                        pendingDays();
                    } else {
                        if (result != null) {
                            if (result.isEmpty()) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        "¡Te faltan todos los días!",
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        }

                    }

                }
            });
            //Desconectarte de la aplicacion. Creamos un dialogo para asegurarnos. Borramos el id
            //del usuario de las shared preferences, para evitar el logueo instantaneo.
        } else if (item.getItemId() == R.id.logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.wanna_logout))
                    .setPositiveButton(getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences sharedPreferences = getSharedPreferences(
                                    MainActivity.MY_PREFERENCES,
                                    MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.commit();
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.no),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.create().show();
        } else if (item.getItemId() == R.id.total) {
            startActivity(new Intent(getApplicationContext(), TotalHoursActivity.class)
                .putExtra(MainActivity.USER_ID, userid));
        } else if (item.getItemId() == R.id.stats) {
            startActivity(new Intent(getApplicationContext(), StatisticsActivity.class));
        }
        return true;
    }

    //Funcion que te devuelve true si el mes es par.
    private boolean isPair() {
        if (month == 4) {
            return true;
        } else if (month == 6) {
            return true;
        } else if (month == 10) {
            return true;
        } else if (month == 12) {
            return true;
        }
        return false;
    }
    //Funcion que te devuelve true si el mes es impar.
    private boolean isOdd() {
        if (month == 1) {
            return true;
        } else if (month == 3) {
            return true;
        } else if (month == 5) {
            return true;
        } else if (month == 7) {
            return true;
        } else if (month == 8) {
            return true;
        } else if (month == 9) {
            return true;
        } else if (month == 11) {
            return true;
        }
        return false;
    }
    //Funcion para calcular los dias que quedan pendientes.
    //Recorreremos el Array del mes que toque (par o inpar) y que antes arriba hemos ido modificando
    //para restar los dias que tuvieran tareas. Por cada dia, lo añadiremos a un string concatenando
    //el mensaje final.
    private void pendingDays() {
        String pendingDays = "";
        if (isPair()) {
            for (int i = 0; i < pairMonth.size(); i++) {
                pendingDays = pendingDays + String.valueOf(pairMonth.get(i)) + ", ";
            }
            initializePairMonth();
        } else if (isOdd()) {
            for (int i = 0; i < oddMonth.size(); i++) {
                pendingDays = pendingDays + String.valueOf(oddMonth.get(i)) + ", ";
            }
            initializeOddMonth();
        } else {
            for (int i = 0; i < february.size(); i++) {
                pendingDays = pendingDays + String.valueOf(february.get(i)) + ", ";
            }
            initializeFebruary();
        }

        String lastPendingDays = pendingDays.substring(0, pendingDays.length() -2);
        String pendingDaysFinal = lastPendingDays + ".";

        //Creamos un dialogo para mostrar los dias pendientes una vez calculados.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.pending_days_dialog, null);
        builder.setView(dialogView);
        final TextView text = (TextView) dialogView.findViewById(R.id.pending_days_msg);
        text.setText(pendingDaysFinal);
        builder.setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.create().show();

    }
}
