package com.luse.android.followyourcalendar.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;
import com.luse.android.followyourcalendar.models.Task;
import com.luse.android.followyourcalendar.models.TaskProject;
import com.luse.android.followyourcalendar.models.UserProject;
import com.luse.android.followyourcalendar.ui.dialogs.AddHoursDialog;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DayActivity extends AppCompatActivity {
    static String DAY_TAG = "day";
    static String MONTH_TAG = "month";
    static String YEAR_TAG = "year";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.taskSpinner)
    AppCompatSpinner spinner;
    @BindView(R.id.projectSpinner)
    AppCompatSpinner projectSpinner;
    private ArrayList<Task> tasks;
    private Bundle bundle;
    private RecyclerViewAdapter adapter;
    private Activity activity;
    private Api api;
    private int taskId;
    private ArrayList<Integer> proyectIds;
    private ArrayList<Integer> tasksIds;
    private ArrayList<String> projectsList;
    private ArrayList<String> taskList;
    private int projectPos;
    private int taskPos;

    public interface Callback {
        void onSuccess(boolean sucess);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;
        setContentView(R.layout.activity_day);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Establecemos el boton para la navegacion inversa
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Recuperamos los datos que nos llegan de la actividad Calendar: dia, mes, año e id del usuario.
        bundle = getIntent().getExtras();
        final int day = bundle.getInt(DAY_TAG);
        final int month = bundle.getInt(MONTH_TAG);
        final int year = bundle.getInt(YEAR_TAG);
        tasks = new ArrayList<>();
        proyectIds = new ArrayList<>();
        tasksIds = new ArrayList<>();
        projectsList = new ArrayList<>();
        taskList = new ArrayList<>();
        final int user = bundle.getInt(MainActivity.USER_ID);

        api = new Api();

        setTitle(year + " - " + month + " - " + day);

        //Establecemos el adapter para inflar la lista de tareas imputadas
        adapter = new RecyclerViewAdapter(day, month, year, getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        String url = Api.GET_ALL_PROJECTS_USER + user;

        //Llamamos a la API para recibir el listado de los nombres de los proyectos. Por cada uno,
        //guardaremos el nombre y el id. Inflamos los spinners.
        api.getUserProjects(getApplicationContext(), url, new Api.Callback<ArrayList<UserProject>>() {
            @Override
            public void onCompleted(ArrayList<UserProject> result) {
                String url;
                for (UserProject item : result) {
                    url = Api.PROJECTS_USER_URL + item.getProjectid();
                    getProjectList(new Callback() {
                        @Override
                        public void onSuccess(boolean sucess) {
                            setSpinner();
                        }
                    }, url);
                }
            }
        });

        //Siempre que el usuario pulse "+", se añadira la tarea al listado. Y se guardara en la base
        //de datos. Para ello creamos una instancia de la clase Task y llamaremos al metodo "post"
        //de la API.
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int actualMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
                if (month == actualMonth || month == actualMonth -1) {
                    DialogFragment fragment = new AddHoursDialog()
                            .setCallback(new AddHoursDialog.Callback() {
                                @Override
                                public void onSuccess(String beginHour, String endHour) {
                                    final Task task = new Task(spinner.getSelectedItem().toString(),
                                            projectSpinner.getSelectedItem().toString(),
                                            beginHour, endHour);
                                    calculateHours(beginHour, endHour);
                                    task.setHours(calculateHours(beginHour, endHour));
                                    task.setMinutes(calculateMinutes(beginHour, endHour));
                                    task.setDay(day);
                                    task.setMonth(month);
                                    task.setYear(year);
                                    task.setUserId(user);
                                    Log.d("PROJECTID", String.valueOf(proyectIds.get(projectPos)));
                                    task.setprojectID(proyectIds.get(projectPos));
                                    task.setTaskId(tasksIds.get(taskPos));
                                    api.post(task, getApplicationContext(), task.storeName(),
                                            new Api.Callback<Task>() {
                                                @Override
                                                public void onCompleted(Task result) {
                                                    taskId = result.getId();
                                                    tasks.add(task);
                                                    adapter.notifyItemInserted(tasks.size() - 1);
                                                }
                                            });
                                }
                            });
                    fragment.show(getSupportFragmentManager(), AddHoursDialog.TAG);
                    //Evitamos que se puedan guardar tareas en meses posteriores, para evitar errores.
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.dont_save_hours_msg,
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });
    }

    private void getProjectList(final Callback callback, String url) {
        api.getProjectTask(getApplicationContext(), url,
                new Api.Callback<TaskProject>() {
                    @Override
                    public void onCompleted(TaskProject result) {
                        proyectIds.add(result.getId());
                        projectsList.add(result.getName());
                        callback.onSuccess(true);
                    }
                });
    }

    //Metodo para inflar el spinner de tareas dependiendo lo que se seleccione en el spinner de proyectos.
    private void setSpinner() {
        //Inflamos el spinner de proyectos con el array de nombres que ya tenemos de antes.
        ArrayAdapter<String> adapterProject = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_item, projectsList);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        projectSpinner.setAdapter(adapterProject);

        //establecemos un listener al spinner de proyectos. Cada vez que se seleccione uno, se cargaran
        //las tareas asociadas a ese.
        projectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                taskList.clear();
                projectPos = position;
                String url = Api.TASKS_PROJECT_URL + proyectIds.get(position);
                //Recibimos el listado de tareas asociadas al proyecto, mediante la llamada a la API.
                api.getProjectTasks(getApplicationContext(), url,
                        new Api.Callback<ArrayList<TaskProject>>() {
                            @Override
                            public void onCompleted(ArrayList<TaskProject> result) {
                                taskList.clear();
                                for (TaskProject item: result) {
                                    taskList.add(item.getName());
                                    tasksIds.add(item.getId());
                                }
                                //Inflamos el spinner de tareas.
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity,
                                        android.R.layout.simple_spinner_item, taskList);
                                arrayAdapter.setDropDownViewResource(
                                        android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(arrayAdapter);
                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Guardamos la posicion de la tarea en el spinner para despues (a la hora de guardar una tarea
        //querremos el id de la tarea para el tema de las estadisticas).
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                taskPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //Metodo para calcular las horas totales dado una hora inicio y otra fin.
    private int calculateHours(String beginHour, String endHour) {
        String[] parts = beginHour.split(":");
        DateTime startTime = new DateTime()
                .withHourOfDay(Integer.parseInt(parts[0]))
                .withMinuteOfHour(Integer.parseInt(parts[1]));

        parts = endHour.split(":");
        DateTime endTime = new DateTime()
                .withHourOfDay(Integer.parseInt(parts[0]))
                .withMinuteOfHour(Integer.parseInt(parts[1]));

        Period p = new Period(startTime, endTime);

        return p.getHours();

    }

    //Metodo para calcular los minutos totales dado una hora inicio y otra fin.
    private int calculateMinutes(String beginHour, String endHour) {
        String[] parts = beginHour.split(":");
        DateTime startTime = new DateTime()
                .withHourOfDay(Integer.parseInt(parts[0]))
                .withMinuteOfHour(Integer.parseInt(parts[1]));

        parts = endHour.split(":");
        DateTime endTime = new DateTime()
                .withHourOfDay(Integer.parseInt(parts[0]))
                .withMinuteOfHour(Integer.parseInt(parts[1]));

        Period p = new Period(startTime, endTime);

        return p.getMinutes();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else {
            finish();
        }
        return true;
    }

    //El adaptador de la clase.
    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
        Context context;
        String user;
        int userid;
        int day;
        int month;
        int year;
        Api api;

        RecyclerViewAdapter(final int day, int month, int year, Context context) {
            this.context = context;
            this.day = day;
            this.month = month;
            this.year = year;
            this.user = bundle.getString(MainActivity.USER_NAME);
            this.userid = bundle.getInt(MainActivity.USER_ID);
            api = new Api();

            loadItems();
        }

        //Cargamos los datos del dia, en caso de que los hubiera e inflamos la lista.
        void loadItems() {
            String url = Api.USER_DAY_URL + userid + "-" + day + "-" + month + "-" + year;
            api.getTasks(getApplicationContext(), url, new Api.Callback<ArrayList<Task>>() {
                        @Override
                        public void onCompleted(ArrayList<Task> result) {
                            tasks = result;
                            adapter.notifyDataSetChanged();
                        }
                    });
        }

        @Override
        public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,
                    parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
            holder.bind(tasks.get(position));
        }

        @Override
        public int getItemCount() {
            if (tasks == null) {
                return 0;
            } else {
                return tasks.size();
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        public Task getTask(int pos) {
            return tasks.get(pos);
        }

        void removeItem(int pos) {
            String url;
            //en caso de que se haya guardado y acto seguido se quiera borrar, hay
            //que tener en cuenta que no esta el id del task (pq se asigna en la bbdd
            // por eso lo guardamos al guardar la tarea.
            if (tasks.get(pos).getId() == 0) {
                url = Api.DELETE_URL + taskId;
            } else {
                url = Api.DELETE_URL + tasks.get(pos).getId();
            }
            tasks.remove(pos);
            adapter.loadItems();
            api.deleteTask(getApplicationContext(), url, new Api.Callback<String>() {
                @Override
                public void onCompleted(String result) {
                    if (result != null) {
                        if (!result.equals(getString(R.string.zero))){
                            Toast.makeText(
                                    getApplicationContext(),
                                    R.string.deleted,
                                    Toast.LENGTH_LONG
                            ).show();
                        } else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    R.string.error_deleting_task_msg,
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                }
            });
            notifyItemRemoved(pos);
        }

        class ViewHolder extends RecyclerView.ViewHolder implements
                View.OnCreateContextMenuListener, View.OnClickListener,
                MenuItem.OnMenuItemClickListener {
            Task item;
            View view;
            TextView nameTask;

            ViewHolder(View itemView) {
                super(itemView);
                this.view = itemView;
                view.setOnClickListener(this);
                view.setOnCreateContextMenuListener(this);
                nameTask = (TextView) itemView.findViewById(R.id.nameTask);
            }

            void bind(Task task) {
                this.item = task;
                nameTask.setText(task.getBeginTime() + "h - " + task.getEndTime() + "h : " +
                        task.getName() + ".");
            }

            @Override
            public void onCreateContextMenu(ContextMenu menu, View v,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                MenuItem deleteItem = menu.add(R.string.delete_item);
                deleteItem.setTitle(getApplicationContext().getResources()
                        .getString(R.string.delete_item));
                deleteItem.setOnMenuItemClickListener(this);
            }

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int actualMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
                if ((getTask(getAdapterPosition()).getMonth() == actualMonth) ||
                        (getTask(getAdapterPosition()).getMonth() == actualMonth - 1)) {
                    loadItems();
                    removeItem(getAdapterPosition());
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.dont_delete_msg,
                            Toast.LENGTH_SHORT
                    ).show();
                }
                return true;
            }

            @Override
            public void onClick(View v) {

            }
        }
    }
}
