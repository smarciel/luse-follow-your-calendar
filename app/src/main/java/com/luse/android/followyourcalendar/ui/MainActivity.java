package com.luse.android.followyourcalendar.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    //Defino todas las variables necesarias: botones, edittexts...
    public static final String USER_NAME = "username";
    public static final String USER_ID = "id";
    public static final String MY_PREFERENCES = "MyPrefs" ;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.imageView)
    ImageView luseLogo;
    @BindView(R.id.login_layout)
    LinearLayout loginLayout1;
    @BindView(R.id.login_layout2)
    LinearLayout loginLayout2;
    @BindView(R.id.saveButton)
    Button saveButton;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.password)
    EditText password;
    Animation animSlideUp;
    private SharedPreferences sharedPreferences;
    private Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        api = new Api();

        //Establecemos las Shared Preferences para el tema del logueo y la session activa
        sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        //Si el usuario ya estaba logueado de antes, no se le pide loguear de nuevo
        if (sharedPreferences.getInt(USER_ID, 0) != 0) {
            Toast.makeText(
                    getApplicationContext(),
                    getString(R.string.welcome_back_msg) + sharedPreferences.getString(USER_NAME, "")
                            + "!",
                    Toast.LENGTH_SHORT
            ).show();
            startActivity(new Intent(getApplicationContext(), CalendarActivity.class)
                    .putExtra(USER_NAME, sharedPreferences.getString(USER_NAME, ""))
                    .putExtra(USER_ID, sharedPreferences.getInt(USER_ID, 0)));
        }

        //Inicializo la animacion.
        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideup);
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Cuando la animacion termine, hago invisible la primera layout y visible la segunda
                //que tiene los campos para introducir nombre y contraseña
                if (animation == animSlideUp) {
                    loginLayout1.setVisibility(View.GONE);
                    loginLayout2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //la animacion la establezco que comience al pulsa el boton de lo LOGIN
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                luseLogo.startAnimation(animSlideUp);
            }
        });

        //Cuando el usuario, se loguee, abro el calendario.
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String user = username.getText().toString();
                String pass = password.getText().toString();
                if (user.equals("") || pass.equals("")){
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.user_pass_please,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    api.login(user, pass, getApplicationContext(),
                            Api.LOGIN_URL, new Api.Callback<String>() {
                                @Override
                                public void onCompleted(String result) {
                                    if (!result.equals("null")) {
                                        int userid;
                                        userid = Integer.valueOf(result);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString(USER_NAME, user);
                                        editor.putInt(USER_ID, userid);
                                        editor.commit();
                                        Toast.makeText(
                                                getApplicationContext(),
                                                getString(R.string.hello) +
                                                        sharedPreferences.getString(USER_NAME, "")
                                                        + "!",
                                                Toast.LENGTH_SHORT
                                        ).show();

                                        startActivity(new Intent(getApplicationContext(),
                                                CalendarActivity.class)
                                                .putExtra(USER_NAME, username.getText().toString())
                                                .putExtra(USER_ID, userid));
                                    } else {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                R.string.user_pass_bad_msg,
                                                Toast.LENGTH_SHORT
                                        ).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}
