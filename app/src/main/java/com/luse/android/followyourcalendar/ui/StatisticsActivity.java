package com.luse.android.followyourcalendar.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;


import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;
import com.luse.android.followyourcalendar.models.TaskProject;
import com.luse.android.followyourcalendar.models.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

    /*
    * Actividad de la busqueda de Estadisticas, diferentes filtros: proyecto, tarea, usuario, mes y año.
    * Listamos desde el servidor la lista de proyectos, tareas y usuarios y se inflan los spinners.
    * Una vez seleccionado los filtros, con el boton se pasa a la actividad de resultados.
    * Se le pasa como parametro la URL para la llamada de la busqueda en la API.
    */

public class StatisticsActivity extends AppCompatActivity {
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.taskSpinner)
    AppCompatSpinner taskSpinner;
    @BindView(R.id.projectSpinner)
    AppCompatSpinner projectSpinner;
    @BindView(R.id.workerSpinner)
    AppCompatSpinner workerSpinner;
    @BindView(R.id.monthSpinner)
    AppCompatSpinner monthSpinner;
    @BindView(R.id.yearSpinner)
    AppCompatSpinner yearSpinner;
    Api api;
    ArrayList<String> projects;
    ArrayList<TaskProject> projectRes;
    ArrayList<String> tasks;
    ArrayList<TaskProject> taskRes;
    ArrayList<String> workers;
    ArrayList<User> users;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        this.activity = this;
        api = new Api();
        projects = new ArrayList<>();
        tasks = new ArrayList<>();
        workers = new ArrayList<>();
        users = new ArrayList<>();
        projectRes = new ArrayList<>();
        taskRes = new ArrayList<>();

        String urlProjects = Api.ALL_PROJ_URL;
        final String urlTasks = Api.ALL_TASKS_URL;
        final String urlUsers = Api.ALL_USERS;

        api.getProjectTasks(getApplicationContext(), urlProjects,
                new Api.Callback<ArrayList<TaskProject>>() {
                    @Override
                    public void onCompleted(ArrayList<TaskProject> result) {
                        for (TaskProject item : result) {
                            projectRes = result;
                            projects.add(item.getName());
                        }
                        api.getProjectTasks(getApplicationContext(), urlTasks,
                                new Api.Callback<ArrayList<TaskProject>>() {
                            @Override
                            public void onCompleted(ArrayList<TaskProject> result) {
                                for (TaskProject item : result) {
                                    taskRes = result;
                                    tasks.add(item.getName());
                                }
                                api.getAllUsers(getApplicationContext(), urlUsers,
                                        new Api.Callback<ArrayList<User>>() {
                                            @Override
                                            public void onCompleted(ArrayList<User> result) {
                                                users = result;
                                                for (User item : result) {
                                                    workers.add(item.getUsername() + " " +
                                                    item.getSurnames());
                                                }
                                                setSpinners();
                                            }
                                        });
                            }
                        });
                    }
                });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String project = projectSpinner.getSelectedItem().toString();
                String task = taskSpinner.getSelectedItem().toString();
                String user = workerSpinner.getSelectedItem().toString();

                String month = getMonth(monthSpinner.getSelectedItem().toString());
                String year = yearSpinner.getSelectedItem().toString();

                int userID;
                int projectID;
                int taskID;

                if (user.equals(getString(R.string.none))) {
                    userID = 0;
                } else {
                    userID = users.get(workers.indexOf(user)).getId();
                }

                projectID = projectRes.get(projects.indexOf(project)).getId();
                if (task.equals(getString(R.string.none))) {
                    taskID = 0;
                } else {
                    taskID = taskRes.get(tasks.indexOf(task)).getId();
                }

                String url = year + "-" + projectID + "-" + taskID + "-" + userID + "-"
                        + month;

                startActivity(new Intent(getApplicationContext(), StatsResultActivity.class)
                .putExtra(getString(R.string.url), url)
                .putExtra(getString(R.string.uname), user));

            }
        });
    }

    //metodo para devolver el numero del mes.
    private String getMonth(String month) {
        String returnMonth = "0";
        if (month.equals(getString(R.string.january))) {
            returnMonth = String.valueOf(1);
        } else if (month.equals(getString(R.string.february))) {
            returnMonth = String.valueOf(2);
        } else if (month.equals(getString(R.string.march))) {
            returnMonth = String.valueOf(3);
        } else if (month.equals(getString(R.string.april))) {
            returnMonth = String.valueOf(4);
        } else if (month.equals(getString(R.string.may))) {
            returnMonth = String.valueOf(5);
        } else if (month.equals(getString(R.string.june))) {
            returnMonth = String.valueOf(6);
        } else if (month.equals(getString(R.string.july))) {
            returnMonth = String.valueOf(7);
        } else if (month.equals(getString(R.string.august))) {
            returnMonth = String.valueOf(8);
        } else if (month.equals(getString(R.string.september))) {
            returnMonth = String.valueOf(9);
        } else if (month.equals(getString(R.string.october))) {
            returnMonth = String.valueOf(10);
        } else if (month.equals(getString(R.string.november))) {
            returnMonth = String.valueOf(11);
        } else if (month.equals(getString(R.string.december))) {
            returnMonth = String.valueOf(12);
        }
        return returnMonth;
    }

    private void setSpinners() {
        tasks.add(getString(R.string.none));
        workers.add(getString(R.string.none));

        ArrayAdapter<String> adapterProjects = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_item, projects);
        ArrayAdapter<String> adapterTasks = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_item, tasks);
        ArrayAdapter<String> adapterWorkers = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_item, workers);

        adapterProjects.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterTasks.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterWorkers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        projectSpinner.setAdapter(adapterProjects);
        taskSpinner.setAdapter(adapterTasks);
        workerSpinner.setAdapter(adapterWorkers);
    }
}
