package com.luse.android.followyourcalendar.ui;

import android.support.v7.app.AppCompatActivity;
import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;
import com.luse.android.followyourcalendar.ui.adapters.StatsRecyclerAdapter;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/* Actividad para mostrar los resultados de la busqueda de las Estadisticas. Establecemos el adapter
 * (StatsRecyclerAdapter) para el listado y le pasamos la URL, para que haga la llamada e infle la lista.
 */


public class StatsResultActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private Bundle bundle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_result);
        ButterKnife.bind(this);

        bundle = getIntent().getExtras();
        final String url = bundle.getString(getString(R.string.url));
        final String username = bundle.getString(getString(R.string.uname));

        String[] values = url.split("-");

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String title;

        if (username.equals(getString(R.string.none))) {
            title = values[1] + " - " + values[0];
        } else {
            title = username + " - " + values[0];
        }

        setTitle(title);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new StatsRecyclerAdapter(
                getApplicationContext(), 2, 0, Api.GET_STATS + url));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else {
            finish();
        }
        return true;
    }
}
