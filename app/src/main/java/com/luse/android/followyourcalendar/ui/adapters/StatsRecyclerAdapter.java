package com.luse.android.followyourcalendar.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.models.Api;
import com.luse.android.followyourcalendar.models.Task;

import java.util.ArrayList;
import java.util.Calendar;

/*
* Clase que define un Adapter para el tema de las Estadisticas y las Horas Totales.
* El Adapter infla un layout con una imageView y un textView.
* En el caso de las Estadisticas, nos llega como parametro un String (que sera vacio en caso de Horas
* Totales) con la URL desde la cual habra que hacer la llamada a la API.
* Tambien recibimos como posicion la posicion del PagerAdapter:
* - si es 0 -> horas totales del mes
* - si es 1 -> horas totales anuales.
* También tenemos como parametro el id del usuario.
*/

public class StatsRecyclerAdapter extends
        RecyclerView.Adapter<StatsRecyclerAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Task> tasks;
    private int position;
    private int user;
    private Api api;
    private String statsURL;


    public StatsRecyclerAdapter(Context context, int position, int user, String s) {
        this.context = context;
        this.position = position;
        this.user = user;
        tasks = new ArrayList<>();
        statsURL = s;
        api = new Api();
        loadItems();
    }

    //metodo para hacer la llamada a la API y y calcular las horas totales por tarea/proyecto.
    private void loadItems() {
        if (position == 0) {
            //horas del mes.
            String url = Api.MONTH_URL + user + "-" + (Calendar.getInstance().get(Calendar
                    .MONTH) + 1)
            + "-" + Calendar.getInstance().get(Calendar.YEAR);
            api.getTasks(context, url, new Api.Callback<ArrayList<Task>>() {
                @Override
                public void onCompleted(ArrayList<Task> result) {
                    if (result != null && !result.isEmpty()) {
                        for (Task item : result) {
                            if (itExists(item.getName())) {
                                int hours = item.getHours() + tasks.get(getItemPos(item.getName()))
                                                .getHours();
                                tasks.get(getItemPos(item.getName())).setHours(hours);

                                int minutes = item.getMinutes() + tasks.get(getItemPos(item
                                        .getName()))
                                                .getMinutes();
                                tasks.get(getItemPos(item.getName())).setMinutes(minutes);
                                } else {
                                    tasks.add(item);
                                    notifyItemInserted(tasks.size() - 1);
                                }
                            }
                        }
                    }
            });
        } else {
            if (position == 1) {
                //horas totales anuales.
                tasks.clear();
                String url = Api.YEAR_URL + user + "-" + Calendar.getInstance().get(Calendar.YEAR);
                api.getTasks(context, url, new Api.Callback<ArrayList<Task>>() {
                    @Override
                    public void onCompleted(ArrayList<Task> result) {
                        if (result != null && !result.isEmpty()) {
                            for (Task item : result) {
                                if (itExists(item.getName())) {
                                    int hours = item.getHours() + tasks.get(getItemPos(item
                                            .getName()))
                                            .getHours();
                                    tasks.get(getItemPos(item.getName())).setHours(hours);

                                    int minutes = item.getMinutes() + tasks.get(getItemPos(item
                                            .getName()))
                                            .getMinutes();
                                    tasks.get(getItemPos(item.getName())).setMinutes(minutes);
                                } else {
                                    tasks.add(item);
                                    notifyItemInserted(tasks.size() - 1);
                                }
                            }
                        }
                    }
                });
            } else {
                //estadisticas
                tasks.clear();
                api.getTasks(context, statsURL, new Api.Callback<ArrayList<Task>>() {
                    @Override
                    public void onCompleted(ArrayList<Task> result) {
                        if (result != null && !result.isEmpty()) {
                            for (Task item : result) {
                                if (itExists(item.getName())) {
                                    int hours = item.getHours() + tasks.get(getItemPos(item
                                            .getName()))
                                            .getHours();
                                    tasks.get(getItemPos(item.getName())).setHours(hours);

                                    int minutes = item.getMinutes() + tasks.get(getItemPos(item
                                            .getName()))
                                            .getMinutes();
                                    tasks.get(getItemPos(item.getName())).setMinutes(minutes);
                                } else {
                                    tasks.add(item);
                                    notifyItemInserted(tasks.size() - 1);
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    //metodo para coger la posicion de un elemento concreto del ArrayList de tareas.
    private int getItemPos(String name) {
        int pos = tasks.size();
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                pos = i;
            }
        }
        return pos;
    }

    //metodo para verificar si un elemento ya existe en el ArrayList de tareas.
    private boolean itExists(String name) {
        boolean exists = false;
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                exists = true;
            }
        }
        return exists;
    }

    //metodo que devuelve una instancia del ViewHolder, creando primero la view a traves
    //del layout.
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_total_layout,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(tasks.get(position));
    }

    @Override
    public int getItemCount() {
        if (tasks == null) {
            return 0;
        } else {
            return tasks.size();
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /*El Adapter hereda de esta clase (que a su ver hereda de una genérica del RecyclerView)
    para inflar el RecyclerView(listado) a su vez con views de tipo "row_total_layout" que tienen
    una imageView y un textView.
    */
    class ViewHolder extends RecyclerView.ViewHolder {
        Task item;
        View view;
        TextView nameTask;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            nameTask = (TextView) itemView.findViewById(R.id.nameTask);
        }

        //metodo para inflar los campos del view.
        void bind(Task task) {
            this.item = task;
            nameTask.setText(task.getProject() + " - " + task.getName() + ": " +
                    getTotalHours(task.getName()));
        }
    }

    private String getTotalHours(String name) {
        int totalHours = 0;
        int totalMinutes = 0;
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                totalHours += tasks.get(i).getHours();
                totalMinutes += tasks.get(i).getMinutes();
            }
        }

        while (totalMinutes >= 60) {
            totalHours += 1;
            totalMinutes -= 60;
        }

        if (totalMinutes != 0) {
            return String.valueOf(totalHours) +  context.getString(R.string.hours_and_msg)
                    + String.valueOf(totalMinutes) + " " + context.getString(R.string.minutos)
                    + context.getString(R.string.dot);
        } else {
            return String.valueOf(totalHours) +  context.getString(R.string.hours_msg)
                    + context.getString(R.string.dot);
        }
    }
}
