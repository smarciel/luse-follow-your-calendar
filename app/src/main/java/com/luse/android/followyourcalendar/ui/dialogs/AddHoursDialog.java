package com.luse.android.followyourcalendar.ui.dialogs;


import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.luse.android.followyourcalendar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sarah-luse on 5/7/16.
 *
 * Esta clase sirve para crear el dialogo a la hora de añadir horas a una tarea.
 * Se compone de dos spinners (hora inicio/hora fin) y dos botones.
 *
 * Interfaz Callback para gestionar la vuelta de datos del dialogo a la actividad.
 */

public class AddHoursDialog extends DialogFragment {

    @BindView(R.id.beginHourSpinner)
    AppCompatSpinner beginHours;
    @BindView(R.id.endHourSpinner)
    AppCompatSpinner endHours;
    private Callback callback;
    public static final String TAG = AddHoursDialog.class.getCanonicalName();


    public interface Callback {
        void onSuccess(String beginHour, String endHour);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.hours_dialog, null);
        ButterKnife.bind(this, view);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            if (beginHours.getSelectedItemPosition() >=
                                    endHours.getSelectedItemPosition()) {
                                Toast.makeText(
                                        getContext(),
                                        R.string.bad_begin_hour_message,
                                        Toast.LENGTH_SHORT
                                ).show();
                                dialog.dismiss();
                            } else {
                                callback.onSuccess(beginHours.getSelectedItem()
                                                .toString(),
                                        endHours.getSelectedItem()
                                                .toString());
                            }
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
    }

    public AddHoursDialog setCallback(Callback callback) {
        this.callback = callback;
        return this;
    }
}
