package com.luse.android.followyourcalendar.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luse.android.followyourcalendar.R;
import com.luse.android.followyourcalendar.ui.adapters.StatsRecyclerAdapter;

/*
 *Clase que hereda de la generica Fragment para inflar el ViewPager. Por cada posicion (0 y 1),
 * se instanciara esta clase. Se le pasan los argumentos necesarios para el Adapter despues.
 *
 * Creamos la view en la que esta definida la recyclerview, la inflamos y le asignamos el adapter
 * que despues inflará la lista.
 */

public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String POSITION = "position";
    private static final String USER = "user";
    private int position;
    private int user;

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int sectionNumber, int position, int user) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putInt(POSITION, position);
        args.putInt(USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(POSITION);
            user = getArguments().getInt(USER);
        }
    }

    //Aqui creamos la view y le asignamos el adapter al RecyclerView.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_total_hours, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new StatsRecyclerAdapter(getContext(), position, user, ""));
        }
        return view;
    }
}
